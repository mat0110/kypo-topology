- name: Install required system packages
  apt:
    pkg:
    - ufw
    - apt-transport-https
    - ca-certificates
    - curl
    - software-properties-common
    - python3-pip
    - virtualenv
    - python3-setuptools
    state: latest
    update_cache: true

- name: Add Docker GPG apt Key
  apt_key:
    url: https://download.docker.com/linux/ubuntu/gpg
    state: present

- name: Add Docker Repository
  apt_repository:
    repo: deb https://download.docker.com/linux/ubuntu focal stable
    state: present

- name: Update apt and install docker-ce
  apt:
    name: docker-ce
    state: latest
    update_cache: true

- name: Install Docker Module for Python
  pip:
    name: docker

- name: Enable docker
  service:
    name: "docker.service"
    enabled: true

- name: Start docker
  service:
    name: "docker.service"
    state: started

- name: Pull default Docker image
  docker_image:
    name: "mailserver/docker-mailserver"
    source: pull

# Based on https://github.com/hmlkao/ansible-docker-mailserver
- name: Create persistent storage folders
  become: true
  file:
    recurse: yes
    path: "{{ item }}"
    state: directory
  with_items:
  - "/opt/mail"
  - "/opt/mail/config"
  - "/opt/mail/maildata"
  - "/opt/mail/mailstate"
  - "/opt/mail/maillogs"

- name: Configure accounts
  template:
    dest: "/opt/mail/config/postfix-accounts.cf"
    src: postfix-accounts.cf.j2
    mode: 0600

- name: Start mail server
  docker_container:
    name: mail-server
    restart_policy: unless-stopped
    image: "mailserver/docker-mailserver"
    volumes:
    - "/opt/mail/config:/tmp/docker-mailserver:ro"
    - "/opt/mail/maildata:/var/mail"
    - "/opt/mail/mailstate:/var/mail-state"
    - "/opt/mail/maillogs:/var/log/mail"
    env:
      ONE_DIR: "1"
      POSTMASTER_ADDRESS: "postmaster@{{ domain_name }}"
      OVERRIDE_HOSTNAME: "mail.{{ domain_name }}"
    published_ports:
    - "25:25"
    - "587:587"
    - "143:143"
    capabilities:
    - NET_ADMIN
    - SYS_PTRACE

- name: UFW - Allow SMTP connections
  ufw:
    rule: allow
    port: 25
    proto: tcp

- name: UFW - Allow SMTP connections
  ufw:
    rule: allow
    port: 587
    proto: tcp

- name: UFW - Allow IMAP connections
  ufw:
    rule: allow
    port: 143
    proto: tcp

