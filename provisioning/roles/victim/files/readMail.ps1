# Import the OpenPop.NET library
Add-Type -Path "OpenPop.dll"

# Create a new instance of the Pop3Client class
$pop3Client = New-Object OpenPop.Pop3.Pop3Client

# Connect to the mail server
$pop3Client.Connect("mail.example.com", 110, $false)

# Authenticate with the mail server
$pop3Client.Authenticate("username", "password")

# Get the number of emails in the mailbox
$emailCount = $pop3Client.GetMessageCount()

# Get the last email from the mailbox
$message = $pop3Client.GetMessage($emailCount)

# Display the subject and body of the last email
Write-Host "Subject: " $message.Headers.Subject
Write-Host "Body: " $message.FindFirstPlainTextVersion().GetBodyAsText()

# Disconnect from the mail server
$pop3Client.Disconnect()