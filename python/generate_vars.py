import argparse
import json
import os
import random
import ipaddress

from shutil import copy2, rmtree
from subprocess import Popen
from jinja2 import Environment, FileSystemLoader

from typing import Dict, List, Any

from pprint import pprint

# Some global variables
output_directory = 'out'
keys_directory = 'keys'


def validate_config(config: dict) -> bool:
    """Validates the configuration of the network

    Args:
        config (dict): Represents the configuration file that needs to be parsed

    Returns:
        bool: Whether or not the config is valid
    """
    # We must validate that the connections run both ways
    for name, values in config.items():
        if "ip" not in values.keys():
            print(f"The range '{name}' has no ip address assigned.\n" +
                  "This ip needs to be set in the 'ip' field.")
            return False
        for conn in values.get("conn_to", []):
            if conn not in config.keys():
                # Check whether the range it refers to actually exists inside the list
                print(
                    f"The range '{name}' tries to make a connection to '{conn}', which is not a valid entry")
                return False
            if name not in config[conn].get("conn_to", []):
                # The connetion is not bi-directionnal
                print(
                    f"The range '{name}' has a connection to '{conn}', which is not bi-directional")
                return False
    return True

def dict_raise_on_duplicates(ordered_pairs):
    """Reject duplicate keys."""
    d = {}
    for k, v in ordered_pairs:
        if k in d:
           raise ValueError("duplicate key: %r" % (k,))
        else:
           d[k] = v
    return d

def parse_file(json_file: str = 'config.json') -> dict:
    """Parse the configuration file

    Args:
        json_file (str, optional): Path to the configuration file. Defaults to 'config.json'.

    Returns:
        dict: representation of the network inside the configuration file as a dictionary
    """
    with open(json_file) as json_data:
        data = json.load(json_data, object_pairs_hook=dict_raise_on_duplicates)
    return data


def generate_key(path: str):
    """Generates an OpenVPN secret key

    Args:
        path (str): Path where the key needs to be stored
    """
    # Generate a static secret key
    proc = Popen(f"openvpn --genkey secret {path}", shell=True)
    (stdout, stderr) = proc.communicate()


def is_valid_tun_ip(local_conf: list, remote_conf: list, tun_ip: str) -> bool:
    """Check if a candidate ip address for the tunnelling interface is valid

    Args:
        local_conf (list): local configuration
        remote_conf (list): remote configuration
        tun_ip (str): candidate ip address for the tunnelling interface

    Returns:
        bool: whether the ip is valid or not
    """
    # Whether we are allocating an ip for the local or remote tun interface, the steps are the same

    # The new ip address can not be allocated to an address that exists on the local machine
    for conn in local_conf:
        if conn.get("tun_ip_local", "") == tun_ip:
            return False

        if conn.get("tun_ip_remote", "") == tun_ip:
            return False

    # The new ip address can not be allocated to an address that exists on the remote machine
    for conn in remote_conf:
        if conn.get("tun_ip_local", "") == tun_ip:
            return False

        if conn.get("tun_ip_remote", "") == tun_ip:
            return False

    return True


def get_assignable_ip(local: list, remote: list, network: ipaddress.IPv4Network) -> str:
    """Returns a valid ip address to assign to the tunnelling interface

    Args:
        local (list): local configuration
        remote (list): remote configuration
        network (ipaddress.IPv4Network): ip range inside which the ip address needs to be generated

    Returns:
        str: valid ip address for the interface
    """
    hosts = list(network.hosts())
    addr = str(random.choice(hosts))

    while not is_valid_tun_ip(local, remote, addr):
        print(f"IP {addr} is not valid")
        addr = str(random.choice(hosts))

    return addr


def get_remote_config(config: Dict[str, List], local_name: str, remote_name: str) -> Dict[str, Any]:
    """Gets the configuration for the connection between the local and remote instance, 
    from the point of view of the remote one

    Args:
        config (Dict[str, List]): global configuration of the network
        local_name (str): name of the local instance
        remote_name (str): name of the remote instance

    Returns:
        Dict[str, Any]: connection describing the connection from the peer's point of view
    """
    for candidate in config[remote_name]:
        if candidate["name"] == f"{remote_name}-{local_name}":
            return candidate


def generate_kypo_config(data: List[Dict[str, Any]], range_name: str):
    """Generates a kypo configuration file

    Args:
        data (List[Dict[str, Any]]): data to write inside the configuration file
        range_name (str): name of the range for which the configuration needs to be generated
    """
    # Get the content of the target file
    content = json.dumps(data, indent=2, separators=(',', ':'))

    # Capitalize the boolean values
    content = content.replace('true', 'True')
    content = content.replace('false', 'False')

    # Output the variables to a file
    with open(f"{output_directory}/{range_name}/kypo_vars.yml", 'w+') as f:
        f.write(content)


def generate_openvpn_config(data: List[Dict[str, Any]], range_name: str):
    """Generate an openvpn configuration file

    Args:
        data (List[Dict[str, Any]]): data to write inside the configuration file
        range_name (str): name of the range for which the configuration needs to be generated
    """
    # Create a Jinja environment
    env = Environment(loader=FileSystemLoader('templates'))

    # Load the template
    template = env.get_template('openvpn_config.j2')

    for connection in data:
        # Render the template
        output = template.render(connection)

        # Output the openvpn config to a file
        with open(f"{output_directory}/{range_name}/{connection['name']}.config", 'w+') as f:
            f.write(output)


def main(config_path: str, fullmesh: bool):
    """Main function.
    Parses the input config file and generates the corresponging KYPO and/or OpenVPN files

    Args:
        config_path (str): Path to the configuration file to parse
        fullmesh (bool): If True, every range will be conneted to each other and the "conn_to" field will be ignored
    """
    
    # Parse the configuration file
    try:
        config = parse_file(config_path)
    except ValueError as e:
        print(f"Error while parsing the json file: {e}")
        return

    # Go through the list for a first pre-processing step
    # If fullmesh is set or the field conn_to is not present
    # Connect the range to every other ranges
    for name in config:
        if fullmesh or "conn_to" not in config[name]:
            # Get a list of all the other ranges in the list
            config[name]["conn_to"] = list(filter(
                lambda x: x != name, list(config.keys())))

    # Check if the configuration file is written correctly
    if not validate_config(config):
        print("ERROR parsing the configuration file. Exiting the program")
        return

    # Create the directory to store the output
    try:
        if os.path.isdir(output_directory):
            rmtree(output_directory)
        os.mkdir(output_directory)
    except OSError as error:
        print(f"An error occured while creating the output directory: {error}")

    out = {}
    for local_name, values in config.items():
        out[local_name] = []

        # Create the directory for this instance
        os.mkdir(f"{output_directory}/{local_name}")
        # Create the directory to store the secret keys
        os.mkdir(f"{output_directory}/{local_name}/{keys_directory}")

        # Iterate over the connections of this particular instance
        for remote_name in values.get("conn_to", []):

            # Generate a unique name for the connection
            connection_name = f"{local_name}-{remote_name}"

            # Dictionary containing the configuration that will be used in KYPO
            connection_data = {"name": connection_name, "extra_options": []}

            # Generate the extra options based on the internal ip ranges of the instance we are connecting to
            remote_ip_range = config[remote_name].get("internal_ip", "")
            if remote_ip_range:
                remote_network = ipaddress.IPv4Network(remote_ip_range)
                connection_data["extra_options"] = [
                    f'route {remote_network.network_address} {remote_network.netmask}']

            if remote_name in out.keys():
                # There is already a connection present
                # Then we must instantiate a client
                connection_data["server"] = False
                remote_config = get_remote_config(out, local_name, remote_name)

                connection_data["tun_ip_local"] = remote_config["tun_ip_remote"]
                connection_data["tun_ip_remote"] = remote_config["tun_ip_local"]
                connection_data["server_port"] = remote_config["server_port"]
                connection_data["remote_ip"] = config[remote_name]["ip"]

                peer_key_name = f"{remote_name}-{local_name}.key"
                copy2(f"{output_directory}/{remote_name}/{keys_directory}/{peer_key_name}",
                      f"{output_directory}/{local_name}/{keys_directory}/{connection_name}.key")

                connection_data["secret_file"] = f"{keys_directory}/{connection_name}.key"
            else:
                # We have a server instance, i.e. the first instance that is created
                connection_data["server"] = True

                # Generate the static OpenVPN key
                generate_key(
                    f"out/{local_name}/{keys_directory}/{connection_name}.key")
                connection_data["secret_file"] = f"{keys_directory}/{connection_name}.key"

                # Generate a random server port
                connection_data["server_port"] = random.randint(10_000, 65_535)

                # Generate a random local ip address for tun interface
                local_cidr_subnet = values.get("tun_ip", "172.0.0.0/24")

                # Create an IP network object from the CIDR subnet
                local_network = ipaddress.ip_network(local_cidr_subnet)

                connection_data["tun_ip_local"] = get_assignable_ip(
                    out.get(local_name, []), out.get(remote_name, []), local_network)

                # Generate a random remote ip address for tun interface
                remote_cidr_subnet = config[remote_name].get(
                    "tun_ip", "172.0.0.0/24")

                # Create an IP network object from the CIDR subnet
                remote_network = ipaddress.ip_network(remote_cidr_subnet)
                connection_data["tun_ip_remote"] = get_assignable_ip(
                    out.get(local_name, []), out.get(remote_name, []), remote_network)

            out[local_name].append(connection_data)

        if values.get("kypo", True):
            # If this is a kypo instance, generate the variables file to put inside the provisioning script
            generate_kypo_config(out[local_name], local_name)
        else:
            # Otherwise, we will generate an openvpn configuration that is ready to be used
            generate_openvpn_config(out[local_name], local_name)

    # pprint(out)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Generates the different configuration files and secret keys for the use inside a federation network using KYPO and OpenVPN')
    parser.add_argument('-c', '--config', default='config.json', required=False, type=argparse.FileType(
        'r'), help='location of the config file to parse')
    parser.add_argument('-f', '--fullmesh', required=False, action='store_true',
                        help='generate the config for a full-mesh network, where every range is connected to every other range')      # option that takes a value
    args = parser.parse_args()

    # Launch main function
    main(config_path=args.config.name, fullmesh=args.fullmesh)
